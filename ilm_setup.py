import random
import argparse
import requests

parser = argparse.ArgumentParser()


def add_life_clycle_policy(elastic_host, policy_name, day_to_delete):
    if elastic_host is None:
        raise Exception("Elastic Search Host is missing")
    if not str(elastic_host).startswith("http://"):
        elastic_host = f"http://{elastic_host}"
    body = {
        "policy": {
            "phases": {
                "hot": {
                    "min_age": "0ms",
                    "actions": {
                        "set_priority": {
                            "priority": None
                        }
                    }
                },
                "delete": {
                    "min_age": f"{day_to_delete}d",
                    "actions": {
                        "delete": {
                            "delete_searchable_snapshot": True
                        }
                    }
                }
            }
        }
    }
    elastic_search_api = f"{elastic_host}/_ilm/policy/{policy_name}"
    headers = {"content-type": "application/json"}
    response = requests.put(elastic_search_api, headers=headers, json=body)
    if response.status_code == 200:
        print(
            f"life cycle policy created successfully with the following info:\npolicy name: {policy_name}\ndelete "
            f"index policy: after {day_to_delete} days")


def add_index_template(elastic_host, policy_name):
    if elastic_host is None:
        raise Exception("Elastic Search Host is missing")

    if not str(elastic_host).startswith("http://"):
        elastic_host = f"http://{elastic_host}"
    body = {
        "index_patterns": [
            "mdclone-*"
        ],
        "priority": 1,
        "template": {
            "settings": {
                "index": {
                    "format": "1",
                    "lifecycle": {
                        "name": f"{policy_name}"
                    }
                }
            }
        },
        "_meta": {
            "managed": True,
            "description": "default metrics template installed by x-pack"
        }
    }
    elastic_search_api = f"{elastic_host}/_index_template/mdclone_template"
    headers = {"content-type": "application/json"}
    response = requests.put(elastic_search_api, headers=headers, json=body)
    if response.status_code == 200:
        print(
            f"\n\nIndex Template created successfully\nTemplate Name:'mdclone_template'\nTemplate name relate To Policy "
            f"Name : {policy_name}")


if __name__ == '__main__':
    parser.add_argument("-host", "--host",
                        dest="hostname",
                        help="Elastic server host")
    parser.add_argument("-name", "--name",
                        dest="policy_name",
                        default=f"DeleteLogPolicy_{random.randint(0, 6000000)}",
                        help="policy name for the life cycle policy")
    parser.add_argument("-days", "--days",
                        dest="days_to_delete", default=100,
                        help="days to delete")
    args = parser.parse_args()

    add_life_clycle_policy(args.hostname, args.policy_name, args.days_to_delete)
    add_index_template(args.hostname, args.policy_name)
