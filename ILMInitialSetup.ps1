﻿function Add-LifeCyclePolicy {
  param (
    $elastic ,
    $PolicyName,
    $daysToDelete
  )  

  if ($elastic -eq $null) {
    Write-Error "ElasticSearch Host is missing"
  } 

  if ($PolicyName -eq $null) {
    $PolicyName = "DeletePolicy_{0}" -f (Get-Random)
    Write-Warning ("PolicyName is not provided set default name to {0}" -f $PolicyName)
  }
  else {
    $validName = ($PolicyName.GetType().Name -ieq "string" )
    if ( -not $validName) {
      Write-Error "Please enter a valid Name: Name must Be a Srting `"<Your_policy_Name>`""
      return
    }
  }

  if ($daysToDelete -eq $null) {
    $daysToDelete = 100
    Write-Warning "policy delete interval set by defualt to 100 days"
  }

  $body = ('{ "policy": { "phases": { "hot": { "min_age": "0ms", "actions": { "set_priority": { "priority": null } } }, "delete": { "min_age": "' + $daysToDelete + 'd", "actions": { "delete": { "delete_searchable_snapshot": true } } } } } }')
  $elasticSearchAPI = ("{0}/_ilm/policy/{1}" -f $elastic, $PolicyName)
  $response = (Invoke-WebRequest -Uri $elasticSearchAPI -Body $body -ContentType application/json -Method PUT | ConvertFrom-Json)
  
  if ( $response.acknowledged -eq 'true' ) {
    Write-Host ( "Index Life Cycle Policy created successfully`nPolicyName:{0}`n" -f $PolicyName) -ForegroundColor Green
  }

  return $PolicyName
}

function Add-IndexTemplate {
  param ( $elastic ,
    $PolicyName)  
  $body = ('{ "index_patterns": [ "mdclone-*" ], "priority": 1, "template": { "settings": { "index": { "format": "1", "lifecycle": { "name": "' + $PolicyName + '" } } } }, "_meta": { "managed": true, "description": "default metrics template installed by x-pack" } }')
  $elasticSearchAPI = ("{0}/_index_template/mdclone_template" -f $elastic)
  $response = (Invoke-WebRequest -Uri $elasticSearchAPI -Body $body -ContentType application/json -Method PUT | ConvertFrom-Json)
  if ( $response.acknowledged -eq 'true' ) {
    Write-Host ("Index Template created successfully`nTemplate Name:{0}`nTemplate name relate To Policy Name : {1}" -f 'mdclone_template', $PolicyName) -ForegroundColor Green
  }
}


$elastic = $args[0]
$Policy = $args[1]
$DaysToDelete = $args[2]

$policytName = Add-LifeCyclePolicy $elastic $Policy $DaysToDelete
Add-IndexTemplate  $elastic $policytName