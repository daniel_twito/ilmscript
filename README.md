﻿



# ILM - Index Lifecycle Management

Elasticsearch give us option to manage index life cycle.
configure its deafferents stages (hot, warm, cold, delete) 

* The script create ILM policy that configure the life cycle of the index.
we configure to have 2 stages, **hot** and **delete**, after X days (for your choose default is 100 days) the index will be moved from hot stage to delete stage and will be deleted from elastic automatically.

* Create an index template with priority 1 to evrey new index with the index pattern ``[mdclone-*]`` the Policy that we create will apply to him.

After running this script, every new index that will be created with the pattern ``mdclone-*`` will be automatically deleted after X days

## Usage Python script

 

**Note: the script was tested using python 3.9**

1. open command line / shell
2. before running the script need to install requests lib using pip
3. run `pip install requests`
4. run the script `ilm_setup.py`  `python ilm_setup.py -host <elastic_host> -name<policy_name> -days<days_to_delete>`
### flags
* -host 
> Elastic host name to connect example http://10.0.2.2:9200 
> you can give the host without the https:// prefix
* -name(Optional)
> The name pf the life cycle policy if not given the name will be DeleteLogPolicy_<random_number> 
* -days(Optional)
> How many day we want that the index will be in the system  after this number of days the index with the pattern `mdclone-*` will be deleted
if not given it will set to 100 days 
### Example
`python ilm_setup.py -host 10.0.2.2:9200`
* We get the following output 
> life cycle policy created successfully with the following info:
>policy name: DeleteLogPolicy_2747328
>delete index policy: after 100 day
> 
> Index Template created successfully
>Template Name:'mdclone_template'
>Template name relate To Policy Name : DeleteLogPolicy_2747328


## Usage PowerShell script:

1. open powerShell
2. in the command line type ``.\ILMInitialSetup.ps1 <ElasticSearch_HOST_Including port> [<Policy_Name_To_Be_Created>] [<Days_To_Delete_From_Elastic>]``

* <ElasticSearch_HOST_Including_port> 
	
	> Elastic search host for exsample http://10.0.2.2:9200
	
* Optional Param: <Policy_Name_To_Be_Created>
   > Will be the Name Of the New Lifecycle Policy if given,
	 if not given will give a random name in this pattern DeletePolicy_<Random_Number>

* Optional Param: <Days_To_Delete_From_Elastic> 

	 >  number of days that after them the index will be deleted.
	  must be an integer the units will always be days
	  


### Example : 
 running the script with all parameters`` .\ILMInitialSetup.ps1 http://10.0.2.2:9200 Test 80``

> this run will create a Lifecycle Policy with the Name **Test** ,
>  configure the policy to **delete index after 80 days**
	in addition we create **index template with the Name mdclone_template** and we bind the Test Lifecycle 			  policy to each index that follow mdclone_template pattern



more examples in the README.docx

